<?php

require __DIR__ . '/../vendor/autoload.php';

$dotenv = new Dotenv\Dotenv( __DIR__ . '/../' );
$dotenv->load();

$apiKey = getenv( 'BREWERYDB_API_KEY' );

$loader = new Twig_Loader_Filesystem( '../templates' );
$twig = new Twig_Environment( $loader, [
    'cache' => '../template_cache',
] );

$redis = new Predis\Client([
    'scheme' => 'tcp',
    'host'   => getenv( 'REDIS_HOST' ),
    'port'   => getenv( 'REDIS_PORT' ),
]);
