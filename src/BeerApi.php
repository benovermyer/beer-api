<?php

namespace App;

use \GuzzleHttp\Client as Client;

class BeerApi {
    private $apiUrl;
    private $client;

    public function __construct( $apiKey, $count = 3, $abv = '+5' ) {
        $this->apiUrl = "http://api.brewerydb.com/v2/beers?format=json&order=random&randomCount=$count&abv=$abv&withBreweries=Y&key=$apiKey";
        $this->client = new Client();
    }

    public function fetch() {
        $res = $this->client->request( 'GET', $this->apiUrl );
        $beerData = json_decode( $res->getBody() );

        return $beerData->data;
    }
}
