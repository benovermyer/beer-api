<?php

require __DIR__ . '/../app/boot.php';

$beerJson = $redis->get( 'beer-data' );

if ( empty( $beerJson ) ) {
    $beerApi = new App\BeerApi( $apiKey );
    $beerData = $beerApi->fetch();
    $beerJson = json_encode( $beerData );
    $redis->set( 'beer-data', $beerJson );
    $redis->expire( 'beer-data', 600 );
} else {
    $beerData = json_decode( $beerJson );
}

echo $twig->render( 'index.twig', [ 'beerData' => $beerData ] );
