# Beers of the Moment

This just reads from the BreweryDB API and presents a set of beers for your perusal.

To use, place a `.env` file in the directory with `index.php`, with your BreweryDB API key defined like this:

```
BREWERYDB_API_KEY="abcdef"
```

You'll also need to define `REDIS_HOST` and `REDIS_PORT` for the caching.
